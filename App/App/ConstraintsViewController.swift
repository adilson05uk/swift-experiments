//
//  ConstraintsViewController.swift
//  App
//
//  Created by Adil Hussain on 27/04/2018.
//  Copyright © 2018 Tazkiya Tech. All rights reserved.
//

import UIKit

class ConstraintsViewController: UIViewController {
    
    @IBOutlet weak var firstLabel: UILabel!
    @IBOutlet weak var secondLabel: UILabel!
    @IBOutlet weak var containerView: UIView!
    
    @IBOutlet weak var containerViewHeightConstraint: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func onFirstButtonTapped(_ sender: Any) {
        let textBefore = firstLabel.text!;
        
        firstLabel.text = textBefore + textBefore;
    }
    
    @IBAction func onSecondButtonTapped(_ sender: Any) {
        let textBefore = secondLabel.text!;
        
        secondLabel.text = textBefore + textBefore;
    }
    
    @IBAction func onThirdButtonTapped(_ sender: Any) {
        
        if (containerViewHeightConstraint.relation == .equal) {
            containerViewHeightConstraint.isActive = false
            containerViewHeightConstraint = containerView.heightAnchor.constraint(greaterThanOrEqualToConstant: 0)
        } else {
            containerViewHeightConstraint.isActive = false
            containerViewHeightConstraint = containerView.heightAnchor.constraint(equalToConstant: 0)
        }
        
        containerViewHeightConstraint.isActive = true
        
        UIView.animate(withDuration: 1.0, animations: {
            self.view.layoutIfNeeded()
        })

    }
    
}
