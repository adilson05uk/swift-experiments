//
//  TableViewCell.swift
//  App
//
//  Created by Adil Hussain on 27/04/2018.
//  Copyright © 2018 Tazkiya Tech. All rights reserved.
//

import UIKit

class TableViewCell: UITableViewCell {

    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var label: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        containerView.layer.cornerRadius = 5
        containerView.layer.masksToBounds = true
    }
    
    public func updateViewsForModel(_ model: Model) {
        containerView.backgroundColor = model.backgroundColor
        label.text = model.text
    }
}
