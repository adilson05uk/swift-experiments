//
//  Model.swift
//  App
//
//  Created by Adil Hussain on 27/04/2018.
//  Copyright © 2018 Tazkiya Tech. All rights reserved.
//

import UIKit

class Model: NSObject {

    let backgroundColor : UIColor;
    let text : String;
    
    init(_ backgroundColor : UIColor, _ text : String) {
        self.backgroundColor = backgroundColor;
        self.text = text;
    }
}
