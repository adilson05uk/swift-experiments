//
//  TableViewController.swift
//  App
//
//  Created by Adil Hussain on 27/04/2018.
//  Copyright © 2018 Tazkiya Tech. All rights reserved.
//

import UIKit

class TableViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var tableView: UITableView!
    
    private var data: [Model] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        data.append(Model(UIColor.black, "Black"))
        data.append(Model(UIColor.blue, "Blue"))
        data.append(Model(UIColor.brown, "Brown"))
        data.append(Model(UIColor.green, "Green"))
        data.append(Model(UIColor.orange, "Orange"))
        data.append(Model(UIColor.purple, "Purple"))
        
        tableView.register(UINib(nibName: "TableViewCell", bundle: nil), forCellReuseIdentifier: "cellReuseIdentifier")
        
        tableView.dataSource = self;
        tableView.delegate = self;
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellReuseIdentifier") as! TableViewCell
        
        let model = data[indexPath.row]
        
        cell.updateViewsForModel(model)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let alertController = UIAlertController(title: "Hint", message: "You have selected row \(indexPath.row).", preferredStyle: .alert)
        
        let alertAction = UIAlertAction(title: "Ok", style: .cancel, handler: nil)
        
        alertController.addAction(alertAction)
        
        present(alertController, animated: true, completion: nil)
    }
    
}
