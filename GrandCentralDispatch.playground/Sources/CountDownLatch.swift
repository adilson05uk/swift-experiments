import Foundation
import Dispatch

public class CountDownLatch {
    
    private let remainingJobs: AtomicInteger
    private let lock = DispatchSemaphore(value: 0)
    
    public init(count: Int) {
        remainingJobs = AtomicInteger(value: count)
    }
    
    public func countDown() {
        remainingJobs.decrement()
        
        if (remainingJobs.value == 0) {
            lock.signal()
        }
    }
    
    public func await() {
        lock.wait()
    }
    
}
