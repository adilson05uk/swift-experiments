import Foundation
import Dispatch

public class AtomicInteger {
    
    private let queue: DispatchQueue = DispatchQueue(label: "LockingQueue")
    private var _value: Int
    
    public init(value: Int) {
        self._value = value;
    }
    
    public var value: Int {
        get {
            return queue.sync { _value }
        }
        set {
            queue.sync { _value = newValue }
        }
    }
    
    public func decrement() -> Void {
        queue.sync { _value = _value - 1 }
    }
    
    public func increment() -> Void {
        queue.sync { _value = _value + 1 }
    }
}

extension AtomicInteger : CustomStringConvertible {
    
    public var description: String {
        return "<\(type(of: self)): value = \(value)>"
    }
}
