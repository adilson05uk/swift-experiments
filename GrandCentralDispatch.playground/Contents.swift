import Dispatch

// MARK: Experiment scheduling work with DispatchQueue and waiting on completion with DispatchGroup

let count = AtomicInteger(value: 1)

let startMessage1 = "Starting DispatchGroup experiment with value of count = \(count.value)"

let group = DispatchGroup()

group.enter()

DispatchQueue.global().async {
    count.increment()
    group.leave()
}

group.wait()

let endMessage1 = "Finishing DispatchGroup experiment with value of count = \(count.value)"

// MARK: Experiment scheduling work with DispatchQueue and waiting on completion with DispatchSemaphore

let startMessage2 = "Starting DispatchSemaphore experiment with value of count = \(count.value)"

let countDownLatch = CountDownLatch(count: 3)

func doSomeWorkAsynchronously(_ countDownLatch: CountDownLatch) {
    DispatchQueue.global().async {
        print("Line 1")
        count.increment()
        print("Line 2")
        countDownLatch.countDown()
        print("Line 3")
    }
}

doSomeWorkAsynchronously(countDownLatch)
doSomeWorkAsynchronously(countDownLatch)
doSomeWorkAsynchronously(countDownLatch)

countDownLatch.await()

print("Line 4")

let endMessage2 = "Finishing DispatchSemaphore experiment with value of count = \(count.value)"

print("Line 5")
