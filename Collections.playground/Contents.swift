import Foundation

var numbersArray: Array = [1, 2, 4, 3, 4, 5]
print("original numbers array = \(numbersArray)")
numbersArray.bringForward { $0 == 4 }
print("numbers array after bringing forward the 4s = \(numbersArray)")
print()

numbersArray = [4, 3, 4, 5]
print("original numbers array = \(numbersArray)")
numbersArray.bringForward { $0 == 4 }
print("numbers array after bringing forward the 4s = \(numbersArray)")
print()

numbersArray = [1, 2, 4, 3, 4, 5]
print("original numbers array = \(numbersArray)")
numbersArray.gather(at: 0) { $0 == 4 }
print("numbers array after gathering the 4s at index 0 = \(numbersArray)")
print()

numbersArray = [1, 2, 4, 3, 4, 5]
print("original numbers array = \(numbersArray)")
numbersArray.gather(at: 3) { $0 == 4 }
print("numbers array after gathering the 4s at index 3 = \(numbersArray)")
print()

numbersArray = [1, 2, 4, 3, 4, 5]
print("original numbers array = \(numbersArray)")
numbersArray.gather(at: 5) { $0 == 4 }
print("numbers array after gathering the 4s at index 5 = \(numbersArray)")
print()

numbersArray = [1, 2, 4, 3, 4, 5]
print("original numbers array = \(numbersArray)")
numbersArray.gather(at: 6) { $0 == 4 }
print("numbers array after gathering the 4s at index 6 = \(numbersArray)")
print()

var array: Array = [1, 2, 3, 4]
print("original array = \(array)")

array.first
array.secondElement
array.last
array.startIndex
array.endIndex
array.count
array.index(before: 2)
array.index(after: 2)
var firstHalfOfArray = array.firstHalf()
var secondHalfOfArray = array.secondHalf()

let transformedArray = array.everyOther() { $0 * 10 }
print("array returned by 'everyOther' function = \(transformedArray)")

firstHalfOfArray.replaceSubrange(0...1, with: [10, 20, 30])
print("'firstHalfOfArray' after calling 'replaceSubrange' function = \(firstHalfOfArray)")

print("original array = \(array)")
print()

let emptyArray: [Int] = []
emptyArray.first
emptyArray.secondElement
emptyArray.last
emptyArray.startIndex
emptyArray.endIndex
emptyArray.count
emptyArray.firstHalf()
emptyArray.secondHalf()
emptyArray.everyOther() { $0 * 10 }

let set: Set = [ "apple", "banana", "carrot", "date" ]
set.first
set.secondElement
set.startIndex
set.endIndex
set.count

let transformedSet = set.everyOther() { $0.capitalized }
print("transformedSet = \(transformedSet)")
print()

let lazySet = set.lazy.filter {
    print("Checking \($0) in lazy function")
    return $0.contains("e")
}
print("created 'lazySet'")
let filteredSet = Set(lazySet)
print("created 'filteredSet'")
print("filteredSet = \(filteredSet)")
print()

let nonLazyCollection = (1...10).map { (it: Int) -> Int in
    print("Checking \(it) in non-lazy map function")
    return it * 2
}.filter { (it: Int) -> Bool in
    print("Checking \(it) in non-lazy filter function")
    return it < 10
}
print("first item in nonLazyCollection = \(nonLazyCollection.first!)")
print("last item in nonLazyCollection = \(nonLazyCollection.last!)")
print()

let lazyCollection = (1...10).lazy.map { (it: Int) -> Int in
    print("Checking \(it) in lazy map function")
    return it * 2
}.filter { (it: Int) -> Bool in
    print("Checking \(it) in lazy filter function")
    return it < 10
}
print("first item in lazyCollection = \(lazyCollection.first!)")
print("last item in lazyCollection = \(lazyCollection.last!)")
print()

let filteredItems = Array(lazyCollection)
print("filteredItems = \(filteredItems)")
print("first item in filteredItems = \(filteredItems.first!)")
print("last item in filteredItems = \(filteredItems.last!)")
print()

var arrayByValue1 = [1, 2, 3, 4]
var arrayByValue2 = arrayByValue1;
arrayByValue2.append(5)
print("'arrayByValue1' after appending an item to 'arrayByReference2' = \(arrayByValue1)")
print("'arrayByValue2' after appending an item = \(arrayByValue2)")
print()

var arrayByReference1 = NSMutableArray(array: [1, 2, 3, 4])
var arrayByReference2 = arrayByReference1;
arrayByReference2.add(5)
print("'arrayByReference1' after adding an item to 'arrayByReference2' = \(arrayByReference1)")
print("'arrayByReference2' after adding an item = \(arrayByReference2)")
