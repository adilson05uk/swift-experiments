import Foundation

public extension Collection {
    
    public func everyOther(_ transform: (Self.Element) -> Self.Element) -> [Self.Element] {
        var result = [Self.Element]()
        var takeNext = false
        
        var index = self.startIndex
        
        while (index < self.endIndex) {
            if takeNext {
                result.append(transform(self[index]))
            }
            
            takeNext = !takeNext
            index = self.index(after: index)
        }
        
        return result
    }
    
    var secondElement: Element? {
        return dropFirst().first
    }
}
