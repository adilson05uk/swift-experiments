import Foundation

public extension Array {
    
    public func firstHalf() -> ArraySlice<Element> {
        return self.dropLast(count / 2)
    }
    
    public func secondHalf() -> ArraySlice<Element> {
        return self.dropFirst(count / 2)
    }
}

public extension Array {
    
    public mutating func bringForward(elementsSatisfying predicate: (Element) -> Bool) {
        if let i = firstIndex(where: predicate) {
            
            let predecessorIndex = (i == startIndex) ? i : index(before: i)
            
            self[predecessorIndex...].stablePartition { !predicate($0) }
        }
    }
    
    public mutating func gather(at target: Int, allSatisfying predicate: (Element) -> Bool) {
        self[..<target].stablePartition { predicate($0) }
        self[target...].stablePartition { !predicate($0) }
    }
}
