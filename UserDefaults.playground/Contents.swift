import UIKit

let defaults = UserDefaults.standard

defaults.set(0.24, forKey: "Volume")
defaults.set(true, forKey: "MusicOn")
defaults.set("Angela", forKey: "PlayerName")
defaults.set(Date(), forKey: "AppLastOpenedByUser")
defaults.set([1, 2, 3], forKey: "MyArray")
defaults.set(["name" : "Adil"], forKey: "MyDictionary")

let volume = defaults.float(forKey: "Volume")
let musicOn = defaults.bool(forKey: "MusicOn")
let playerName = defaults.string(forKey: "PlayerName")
let appLastOpenedByUser = defaults.object(forKey: "AppLastOpenedByUser")
let myArray = defaults.array(forKey: "MyArray")
let myDictionary = defaults.dictionary(forKey: "MyDictionary")

let myNonExistentArray = defaults.array(forKey: "MyNonExistentArray")
let myNonExistentDictionary = defaults.dictionary(forKey: "MyNonExistentDictionary")
