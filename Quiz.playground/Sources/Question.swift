import Foundation

public class Question {
    
    public var questionText : String
    public var option1 : String
    public var option2 : String
    public var option3 : String
    public var correctAnswer : Int
    
    public init(questionText : String,
                option1 : String,
                option2 : String,
                option3 : String,
                correctAnswer : Int) {
        self.questionText = questionText;
        self.option1 = option1;
        self.option2 = option2;
        self.option3 = option3;
        self.correctAnswer = correctAnswer;
    }
}
