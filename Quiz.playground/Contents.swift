import Foundation

let questions = [
    Question(questionText: "What is the first prayer of the day?",
             option1: "Fajr",
             option2: "Dhuhr",
             option3: "Asr",
             correctAnswer: 1
    ),
    Question(questionText: "What is the second prayer of the day?",
             option1: "Fajr",
             option2: "Dhuhr",
             option3: "Asr",
             correctAnswer: 2
    ),
    Question(questionText: "What is the third prayer of the day?",
             option1: "Fajr",
             option2: "Dhuhr",
             option3: "Asr",
             correctAnswer: 3
    )
]

for currentQuestionIndex in 0...(questions.count - 1) {
    let question = questions[currentQuestionIndex];
    
    print(question.questionText);
    print("Option 1: \(question.option1)")
    print("Option 2: \(question.option2)")
    print("Option 3: \(question.option3)")
    print("The correct answer is Option \(question.correctAnswer)")
    print()
}
