import Foundation

public class Class1 {
    
    public init() {}
    
    private func sayHello() -> String {
        return "Hello";
    }
    
    public func sayHello(name: String) -> String {
        return sayHello() + " " + name;
    }
}
