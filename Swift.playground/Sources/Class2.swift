import Foundation

public class Class2 {
    
    private var names = [String]()
    
    public init() {}
    
    public func sayHello(index : Int) -> String {
        return Class1().sayHello(name: names[index])
    }
    
    public func addName(name: String) -> Void {
        names.append(name)
    }
}
