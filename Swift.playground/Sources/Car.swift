import Foundation

public class Car {
    
    public var destination : String?
    
    public init(destination : String?) {
        self.destination = destination;
    }
}
