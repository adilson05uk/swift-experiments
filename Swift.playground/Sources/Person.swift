import Foundation

public class Person {
    
    public var name : String = "Unspecified";
    public var gender : Gender = Gender.Unspecified;
    
    public init() {}
    
    public convenience init(name: String, gender: Gender) {
        self.init()
        
        self.name = name;
        self.gender = gender;
    }
}
