import Foundation

// Experiments with classes

let class1 : Class1 = Class1();
class1.sayHello(name: "Adil")

let class2 : Class2 = Class2();
class2.addName(name: "Adil")
class2.sayHello(index: 0)

let person1 = Person(name:"Alice", gender:Gender.Famale);
person1.name
person1.gender

let person2 = Person()
person2.name
person2.gender

let car1 = Car(destination: "Somewhere")
car1.destination!

// Experiments with let

if let carDestination = car1.destination {
    print("car1 driving towards \(carDestination)")
} else {
    print("car1 driving towards nowhere!")
}

let car2 = Car(destination: nil)

if let carDestination = car2.destination {
    print("car2 driving towards \(carDestination)")
} else {
    print("car2 driving towards nowhere!")
}

// Experiments with Dictionaries

let myDictionary : [String : String] = ["key1" : "value1", "key2" : "value2"];
myDictionary["key1"]
myDictionary["key3"]

// Experiments with NSArrays

var nsArray1 : NSMutableArray = ["A", "B", "C"];
var nsArray2 : NSArray = nsArray1;

nsArray1.add("A")

var nsArray3 = nsArray1
var nsArray4 = nsArray2

// Experiments with Arrays

var array1 : Array = ["A", "B", "C"];
var array2 = array1;

array1.append("D")

var array3 = array1
var array4 = array2

// Experiments with as

let convertedNSArray1 : NSArray = nsArray1 as NSArray;
let convertedNSArray2 : NSMutableArray = nsArray2 as! NSMutableArray;

// Experiments with == and ===

"Hello" == "Hello"
"Hello" == NSString(string: "Hello")
NSString(string: "Hello") == NSMutableString(string: "Hello")
NSString(string: "Hello") === NSMutableString(string: "Hello")
NSString(string: "Hello") == NSString(string: "Hello")
NSString(string: "Hello") === NSString(string: "Hello") // expected this to be false but it's true; maybe because of "interning"?

// Experiments with functions and closures

func calculator (n1: Int, n2: Int, operation: (Int, Int) -> Int) -> Int {
    return operation(n1, n2)
}

func add(n1: Int, n2: Int) -> Int {
    return n1 + n2
}

calculator(n1: 2, n2: 3, operation: add)
calculator(n1: 2, n2: 3, operation: { (n1: Int, n2: Int) -> Int in return n1 + n2 })
calculator(n1: 2, n2: 3, operation: { (n1, n2) in return n1 + n2 })
calculator(n1: 2, n2: 3, operation: { $0 + $1 })
calculator(n1: 2, n2: 3) { $0 + $1 }

let originalNumbersArray = [1, 4, 3, 2, 5]
let modifiedNumbersArray = originalNumbersArray.sorted(by: <).map({ $0 + 1}).filter({ $0 % 2 == 0}).map({ "\($0)" })

print(originalNumbersArray)
print(modifiedNumbersArray)

var sum1 = originalNumbersArray.reduce(0, { $0 + $1 })
var sum2 = originalNumbersArray.reduce(0, +)
print(sum1)
print(sum2)

var doubleInputV1: (Int) -> (Int) = { x in return 2 * x }
var doubleInputV2: (Int) -> (Int) = { return 2 * $0 }
var doubleInputV3 = { return 2 * $0 }
var doubleInputV4 = doubleInputV1

doubleInputV1(2)
doubleInputV2(2)
doubleInputV3(2)
doubleInputV4(2)

var globalNumber = 0

var incrementGlobalNumber = {
    globalNumber += 1;
    print("globalNumber = \(globalNumber)")
}

var incrementGlobalNumberV1 = incrementGlobalNumber
var incrementGlobalNumberV2 = incrementGlobalNumber

incrementGlobalNumberV1()
incrementGlobalNumberV2()

func makeIncrementLocalNumberClosure() -> () -> () {
    var localNumber = 0
    return {
        localNumber += 1;
        print("localNumber = \(localNumber)")
    }
}

var incrementLocalNumberV1 = makeIncrementLocalNumberClosure()
var incrementLocalNumberV2 = makeIncrementLocalNumberClosure()

incrementLocalNumberV1()
incrementLocalNumberV2()

// Experiments with array functions

let numbers = [1, 2, 3, 5, 4, 6, 7, 8, 9, 3, 12, 11, 10]
let maxNumber = numbers.reduce(0, { max($0, $1) })
print(maxNumber)

// Experiments with ranges

let strings = ["a", "b", "c", "d", "e"]
var concatenatedStrings = strings.reduce("", { $0 + " " + $1 }).dropFirst()
var startIndex = concatenatedStrings.index(concatenatedStrings.startIndex, offsetBy: 2)
var endIndex = concatenatedStrings.index(concatenatedStrings.endIndex, offsetBy: -2)
var range = startIndex..<endIndex
var choppedString = concatenatedStrings[range]
print(concatenatedStrings)
print(choppedString)

// Experiments with defer

func testDefer() {
    defer { print("Bye") }
    print("Hello")
}

testDefer()

// Experiments with throw and catch

enum MyFirstError: Error {
    case boohoo
}

func throwingFunction() throws {
    throw MyFirstError.boohoo
}

func catchingFunction() {
    do {
        try throwingFunction()
    } catch {
        print("Caught MyFirstError.boohoo")
    }
}

catchingFunction()
